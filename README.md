# Small Web Utilities

Some small client-side programs that can aid you, in your browser. Every one of them is made by hand in plain old HTML, with pure JavaScript. Also none of them contain very fancy styling.
